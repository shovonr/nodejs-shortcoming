require("some-useless-module");
var fn = require("hello-world-helper");

// The console should show version 1.0.0, and 0.0.0, respectively, but that's
// not the case, since String is global, and one of the modules are messing
// around with things.

console.log(String.prototype.someuselessmodule.version);
fn();